import Link from 'next/link';
import Image from 'next/image';
import React, {useState} from 'react';
import styles from '../styles/HeaderBar.module.css';

export default function HeaderBar() {
  const [loggedIn, setLoggedIn] = useState(false);

  const toggleStatus = () => {
    setLoggedIn(!loggedIn);
  };

  return (
    <div className={styles.container}>
      <Link href="/Career">
        <span className={styles.headerIcon_text}>Career</span>
      </Link>
      <Link href="/Products">
        <span
          style={{
            minWidth: '14%',
            justifyContent: 'space-between',
          }}
          className={styles.headerIcon_text}>
          Products
          <Image src={require('../styles/icons/Icon-Arrow-down.svg')} />
        </span>
      </Link>
      <Link href="/Pricing">
        <span className={styles.headerIcon_text}>Pricing</span>
      </Link>
      <Link href="/About">
        <span className={styles.headerIcon_text}>About</span>
      </Link>
      <span className={styles.headerIcon_icon}>
        <Image src={require('../styles/icons/Search.svg')} />
      </span>
      {loggedIn ? (
        <>
          <span className={styles.headerIcon_icon}>
            <Image src={require('../styles/icons/Cart.svg')} />
          </span>
          <span
            onClick={() => setLoggedIn(!loggedIn)}
            className={styles.headerIcon_icon}>
            <Image
              height="26"
              width="30"
              src={require('../styles/icons/user_avatar.svg')}
            />
          </span>
        </>
      ) : (
        <span
          onClick={toggleStatus}
          className={styles.headerIcon_text}
          id="login">
          Login
        </span>
      )}
      <span id="bars_icon" onClick={() => console.log('pressed!')}>
        <Image src={require('../styles/icons/Search.svg')} />
      </span>
      <style jsx>{`
        #bars_icon {
          display: none;
        }
        @media (max-width: 1000px) {
          #login {
            display: none;
          }
          #bars_icon {
            display: flex;
          }
        }
      `}</style>
    </div>
  );
}
